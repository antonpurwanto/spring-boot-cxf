package com.example.demo.cxf;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.demo.config.ApplicationTestConfiguration;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DemocxfwsApplicationTests {

	@Test
	public void getId() {
		HelloWs hello = new HelloWs();
	
		 assertEquals(3, hello.tambah(1, 2));

	}

}