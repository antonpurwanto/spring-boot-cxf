package com.example.demo.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.example.demo.cxf.soap.OrangWs;
import com.example.demo.cxf.soap.OrangWsImpl;

@Configuration
public class ApplicationTestConfiguration {

	@Bean
	public OrangWs orangService() {
		return new OrangWsImpl();
	}
}