package com.example.demo.cxf;

import javax.jws.WebMethod;
import javax.jws.WebService;

import org.apache.cxf.feature.Features;

@WebService
@Features(features="org.apache.cxf.feature.LoggingFeature")
public class HelloWs {
	@WebMethod
	public int tambah(int a, int b) {
		int c=a+b;
		return a+b;
	}

}
