package com.example.demo.cxf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemocxfwsApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(DemocxfwsApplication.class, args);
	}
	
}
