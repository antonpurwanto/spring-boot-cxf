package com.example.demo.cxf.config;

import javax.xml.ws.Endpoint;

import org.apache.cxf.Bus;
import org.apache.cxf.bus.spring.SpringBus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.apache.cxf.transport.servlet.CXFServlet;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.example.demo.cxf.HelloWs;
import com.example.demo.cxf.soap.OrangWs;
import com.example.demo.cxf.soap.OrangWsImpl;

@Configuration
public class WebServicesConfiguration {

	public static final String BASE_URL = "/soap-api";
	public static final String TAMBAH_URL = "/tambahWs";
	public static final String ORANG_URL = "/orangWs";

	
	@Bean
	public ServletRegistrationBean cxfServlet() {
		return new ServletRegistrationBean(new CXFServlet(), BASE_URL + "/*");
	}

	@Bean(name = Bus.DEFAULT_BUS_ID)
	public SpringBus springBus() {
		return new SpringBus();
	}
	
	@Bean
	public HelloWs HelloService() {
		return new HelloWs();
	}
	
	@Bean
	public OrangWs orangService(){
		return new OrangWsImpl();
	}
	
	@Bean
	public Endpoint endpointTest() {

		Endpoint endpoint = new EndpointImpl(springBus(), HelloService());
		endpoint.publish(TAMBAH_URL);
		return endpoint;

	}

	@Bean
	public Endpoint endpointCrud() {

		Endpoint endpoint = new EndpointImpl(springBus(), orangService());
		endpoint.publish(ORANG_URL);
		return endpoint;

	}
}
