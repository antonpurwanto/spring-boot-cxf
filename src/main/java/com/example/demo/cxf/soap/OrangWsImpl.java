package com.example.demo.cxf.soap;

import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.cxf.entities.Orang;
import com.example.demo.cxf.repositories.OrangRepository;
import com.example.demo.cxf.soap.exception.customException;
@Service
public class OrangWsImpl implements OrangWs {
	
	private static Logger logger= LogManager.getLogger(OrangWsImpl.class);
	
	@Autowired 
	private OrangRepository orangRepo;

	public Orang createOrang(Orang orang) {
		if(orang.getId()==null || orang.getId().length()==0) {
			throw new RuntimeException("Id tidak null");
		}
		return orangRepo.save(orang);
	}

	public Boolean isUpdateOrang(Orang orang) throws Exception {
		if(orang.getId()==null || orang.getId().length()==0) {
			throw new Exception("Id tidak null");
		}
		Orang update = orangRepo.save(orang);
		Boolean ok = update.equals(orang);
		if(ok== false) {
			return true;
		}
		return ok ;
	}

	public Orang getOrang(String id) throws customException  {
		boolean data = orangRepo.existsById(id);
		if(!data) {
			throw new customException("data input incorect");
		}
		Orang orang = orangRepo.findById(id).get();
		return orang;
	}

	public List<Orang> getAlls() {
		return orangRepo.findAll();
	}

	public void deleteOrang(String id) {
		orangRepo.deleteById(id);
		
	}

}
