package com.example.demo.cxf.soap;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;

import org.apache.cxf.feature.Features;

import com.example.demo.cxf.entities.Orang;
import com.example.demo.cxf.soap.exception.customException;

@WebService
@Features(features="org.apache.cxf.feature.LoggingFeature")
public interface OrangWs {
	@WebMethod
	Orang createOrang(Orang orang);
	@WebMethod
	Boolean isUpdateOrang(Orang orang) throws Exception;
	@WebMethod
	Orang getOrang(String id) throws customException;
	@WebMethod
	void deleteOrang(String id);
	@WebMethod
	List<Orang> getAlls()throws customException;
	
	

}
