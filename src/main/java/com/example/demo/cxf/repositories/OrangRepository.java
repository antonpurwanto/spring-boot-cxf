package com.example.demo.cxf.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.cxf.entities.Orang;
@Repository
public interface OrangRepository extends JpaRepository<Orang, String> {

}
